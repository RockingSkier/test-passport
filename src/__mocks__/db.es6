'use strict';

// Creating a mock from DB just returns `undefined` so creating a manual one
let db = {};

export default db;

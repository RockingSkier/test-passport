'use strict';

import config from 'config';
import jwt from 'jsonwebtoken';


exports.generate = function (payload, options) {
  options = options || {};
  let secret = config.get('jwt.secret');
  let opts = {
    audience: config.get('jwt.audience'),
    expiresInSeconds: typeof options.expiresIn !== 'undefined' ? options.expiresIn : config.get('jwt.expiresIn'),
    issuer: config.get('jwt.issuer')
  };
  return jwt.sign(payload, secret, opts);
};

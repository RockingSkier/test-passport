'use strict';

import passportAnonymous from 'passport-anonymous';


let AnonymousStrategy = passportAnonymous.Strategy;

export default new AnonymousStrategy();

'use strict';

import db from 'db';


export default function authenticate (jwtPayload, done) {
  if (!jwtPayload.id) { return done(null, false); }

  let UserModel = db.model('User');

  UserModel
    .forge({
      id: jwtPayload.id
    })
    .fetch({
      require: true
    })
    .then(function (user) {
      return done(null, user);
    })
    .catch(UserModel.NotFoundError, function (/*err*/) {
      return done(null, false);
    })
    .catch(function (err) {
      return done(err);
    });
}

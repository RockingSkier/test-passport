'use strict';

import config from 'config';
import passportJwt from 'passport-jwt';

import authenticate from './authenticate';

let opts = {};
opts.secretOrKey = config.get('jwt.secret');
opts.audience = config.get('jwt.audience');
opts.issuer = config.get('jwt.issuer');

let JwtStrategy = passportJwt.Strategy;


export default new JwtStrategy(opts, authenticate);

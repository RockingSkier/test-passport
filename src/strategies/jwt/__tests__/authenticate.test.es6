'use strict';

jest.dontMock('../authenticate');


describe('JWT authenticate', function() {
  let authenticate;
  let catch1;
  let catch2;
  let db;
  let done;
  let model;
  let then;

  beforeEach(function () {
    authenticate = require('../authenticate');
    db = require('db');
    model = {};

    catch2 = jest.genMockFn();
    catch1 = jest.genMockFn().mockReturnValue({ catch: catch2 });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model.forge = jest.genMockFn().mockReturnThis();
    model.fetch = jest.genMockFn().mockReturnValue({ then });
    model.NotFoundError = new Error('User not found');

    db.model = jest.genMockFn().mockReturnValue(model);

    done = jest.genMockFn();
  });

  it('should not query the database when no id is passed through', function () {
    authenticate({ id: null }, done);

    expect(model.fetch).not.toBeCalled();
  });

  it('should return false when no id is passed through', function () {
    authenticate({ id: null }, done);

    expect(done).toBeCalled();
    expect(done).toBeCalledWith(null, false);
  });

  it('should return a user on successful login', function () {
    then = jest.genMockFn().mockImplementation(function (fn) {
      fn(model);
      return { catch: catch1 };
    });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate({ id: 1 }, done);

    expect(done).toBeCalled();
    expect(done).toBeCalledWith(null, model);
  });

  it('should return false on unsuccessful login', function () {
    catch1 = jest.genMockFn().mockImplementation(function (error, fn) {
      fn();
      return { catch: catch2 };
    });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate({ id: -1 }, done);

    expect(done).toBeCalled();
    expect(done).toBeCalledWith(null, false);
  });

  it('should bubble errors', function () {
    let error = new Error('summ\'t when wrawng');
    catch2 = jest.genMockFn().mockImplementation(function (fn) {
      fn(error);
    });
    catch1 = jest.genMockFn().mockReturnValue({ catch: catch2 });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate({ id: 1 }, done);

    expect(done).toBeCalled();
    expect(done).toBeCalledWith(error);
  });

});

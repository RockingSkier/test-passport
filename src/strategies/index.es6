'use strict';

import anonymous from './anonymous';
import jwt from './jwt';
import local from './local';


export default {
  anonymous,
  jwt,
  local
};

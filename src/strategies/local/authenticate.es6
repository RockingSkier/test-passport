'use strict';

import db from 'db';


export default function authenticate (username, password, done) {
  let UserModel = db.model('User');

  UserModel
    .forge({
      username
    })
    .fetch({
      require: true
    })
    .then(function (user) {
      if (password !== user.get('password')) {
        return done(null, false, {
          message: 'username and password combination is invalid'
        });
      }
      return done(null, user);
    })
    .catch(UserModel.NotFoundError, function (/*err*/) {
      return done(null, false, {
        message: 'username and password combination is invalid'
      });
    })
    .catch(function (err) {
      return done(err);
    });
}

'use strict';

jest.dontMock('../authenticate');


describe('Local authenticate', function() {
  let authenticate;
  let catch1;
  let catch2;
  let db;
  let done;
  let model;
  let then;


  beforeEach(function () {
    authenticate = require('../authenticate');
    db = require('db');

    catch2 = jest.genMockFn();
    catch1 = jest.genMockFn().mockReturnValue({ catch: catch2 });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model = {};
    model.forge = jest.genMockFn().mockReturnThis();
    model.fetch = jest.genMockFn().mockReturnValue({ then });
    model.get = jest.genMockFn().mockImplementation(function () {
      return 'password';
    });
    model.NotFoundError = new Error('User not found');

    db.model = jest.genMockFn().mockReturnValue(model);

    done = jest.genMockFn();
  });

  it('should fetch a user with the given username', function () {
    authenticate('username', 'password', done);

    expect(model.forge).toBeCalled();
    expect(model.forge).toBeCalledWith({ username: 'username' });
    expect(model.fetch).toBeCalled();
  });

  it('should validate against the given password', function () {
    then = jest.genMockFn().mockImplementation(function (fn) {
      fn(model);
      return { catch: catch1 };
    });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('username', 'password', done);

    expect(model.get).toBeCalled();
    expect(model.get).toBeCalledWith('password');
  });

  it('should return false if the passwords don\'t match', function () {
    then = jest.genMockFn().mockImplementation(function (fn) {
      fn(model);
      return { catch: catch1 };
    });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('username', 'incorrectPassword', done);

    expect(done).toBeCalled();
    expect(done.mock.calls[0][0]).toEqual(null);
    expect(done.mock.calls[0][1]).toEqual(false);
  });

  it('should return a message if the passwords don\'t match', function () {
    then = jest.genMockFn().mockImplementation(function (fn) {
      fn(model);
      return { catch: catch1 };
    });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('username', 'incorrectPassword', done);

    expect(done).toBeCalled();
    expect(done.mock.calls[0][2].message).toEqual(jasmine.any(String));
  });

  it('should return a user on successful login', function () {
    then = jest.genMockFn().mockImplementation(function (fn) {
      fn(model);
      return { catch: catch1 };
    });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('username', 'password', done);

    expect(done).toBeCalled();
    expect(done).toBeCalledWith(null, model);
  });

  it('should return false if no user is found', function () {
    catch1 = jest.genMockFn().mockImplementation(function (error, fn) {
      fn();
      return { catch: catch2 };
    });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('invalidUsername', 'password', done);

    expect(done).toBeCalled();
    expect(done.mock.calls[0][0]).toEqual(null);
    expect(done.mock.calls[0][1]).toEqual(false);
  });

  it('should return a message if no user is found', function () {
    catch1 = jest.genMockFn().mockImplementation(function (error, fn) {
      fn();
      return { catch: catch2 };
    });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('invalidUsername', 'password', done);

    expect(done).toBeCalled();
    expect(done.mock.calls[0][2].message).toEqual(jasmine.any(String));
  });

  it('should bubble errors', function () {
    let error = new Error('summ\'t when wrawng');
    catch2 = jest.genMockFn().mockImplementation(function (fn) {
      fn(error);
    });
    catch1 = jest.genMockFn().mockReturnValue({ catch: catch2 });
    then = jest.genMockFn().mockReturnValue({ catch: catch1 });

    model.fetch = jest.genMockFn().mockReturnValue({ then });

    authenticate('username', 'password', done);

    expect(done).toBeCalled();
    expect(done).toBeCalledWith(error);
  });

});

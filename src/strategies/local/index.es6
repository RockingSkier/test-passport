'use strict';

import passportLocal from 'passport-local';

import authenticate from './authenticate';


let LocalStrategy = passportLocal.Strategy;


export default new LocalStrategy(authenticate);

'use strict';

import config from 'config';
import bookshelf from 'bookshelf';
import knex from 'knex';

import knexfile from 'knexfile';

let knex_ = knex(knexfile[config.get('databaseConfig')]);
let bookshelf_ = bookshelf(knex_);

export default bookshelf_;

'use strict';

import _ from 'lodash';


export default function (bookshelf) {

  bookshelf.plugin('visibility');

  return bookshelf.Model.extend({
    hasTimestamps: ['createdAt', 'updatedAt'],

    format: function(attrs) {
      // Convert keys to database column names (camelCase -> snake_case)
      return _.reduce(attrs, function(memo, val, key) {
        memo[_.snakeCase(key)] = val;
        return memo;
      }, {});
    },

    parse: function(attrs) {
      // Convert keys from database column names (snake_case -> camelCase)
      return _.reduce(attrs, function(memo, val, key) {
        memo[_.camelCase(key)] = val;
        return memo;
      }, {});
    },

    hidden: [
      'createdAt',
      'updatedAt'
    ]
  });
}

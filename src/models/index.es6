'use strict';

import user from './user';


export default function (bookshelf) {

  return {
    User: user(bookshelf)
  };
}

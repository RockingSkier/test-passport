'use strict';

import jwt from 'jwt';
import baseModel from './base-model';


export default function (bookshelf) {

  bookshelf.plugin('registry');

  let BaseModel = baseModel(bookshelf);

  return bookshelf.model('User') || bookshelf.model('User', BaseModel.extend({
    tableName: 'users',

    defaults: {
      username: null,
      password: null
    },

    generateToken: function () {
      let payload = {
        id: this.get('id')
      };
      return jwt.generate(payload);
    }
  }));
}

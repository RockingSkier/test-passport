'use strict';

let model = {
  forge: jest.genMockFn(),
  fetch: jest.genMockFn()
};

export default model;

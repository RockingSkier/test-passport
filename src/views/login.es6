'use strict';

import passport from 'passport';


export default function login (req, res, next) {
  passport.authenticate('local', { session: false }, function (err, user, info) {
    if (err) { return next(err); }

    if (!user) {
      return res
        .status(400)
        .json(info);
    }

    return res
      .json({
        user: { username: user.get('username')},
        token: user.generateToken()
      });

  })(req, res, next);
}

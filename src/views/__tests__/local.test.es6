'use strict';

jest.dontMock('../login');


describe('login view', function() {
  let login;
  let next;
  let passport;
  let req;
  let res;
  let user;

  beforeEach(function () {
    login = require('../login');
    passport = require('passport');

    user = {
      get: jest.genMockFn().mockReturnValue('username'),
      generateToken: jest.genMockFn().mockReturnValue('token')
    };

    req = {};

    res = {};
    res.json = jest.genMockFn().mockReturnThis();
    res.status = jest.genMockFn().mockReturnThis();

    next = jest.genMockFn();

    passport.authenticate.mockImplementation(function () {
      return jest.genMockFn();
    });
  });

  it('should authenticate with passport', function () {
    login(req, res, next);

    expect(passport.authenticate).toBeCalled();
  });

  it('should specify \'local\' authentication', function () {
    login(req, res, next);

    expect(passport.authenticate.mock.calls[0][0]).toEqual('local');
  });

  it('should specify not to use seesion', function () {
    login(req, res, next);

    expect(passport.authenticate.mock.calls[0][1].session).toBe(false);
  });

  it('should bubble errors', function () {
    let error;
    passport.authenticate.mockImplementation(function (strategy, options, cb) {
      return jest.genMockFn().mockImplementation(function (/*err, user, info*/) {
        error = new Error('bubble');
        cb(error);
      });
    });

    login(req, res, next);

    expect(next).toBeCalled();
    expect(next).toBeCalledWith(error);
  });

  it('should set the status to \'400\' when there is no user', function () {
    passport.authenticate.mockImplementation(function (strategy, options, cb) {
      return jest.genMockFn().mockImplementation(function (/*err, user, info*/) {
        cb(undefined, false);
      });
    });

    login(req, res, next);

    expect(res.status).toBeCalled();
    expect(res.status).toBeCalledWith(400);
  });

  it('should return a message when there is no user', function () {
    passport.authenticate.mockImplementation(function (strategy, options, cb) {
      return jest.genMockFn().mockImplementation(function (/*err, user, info*/) {
        cb(undefined, false, {
          message: 'nope, not logged in'
        });
      });
    });

    login(req, res, next);

    expect(res.json).toBeCalled();
    expect(res.json).toBeCalledWith({
      message: 'nope, not logged in'
    });
  });

  it('should return a user on successful login', function () {
    passport.authenticate.mockImplementation(function (strategy, options, cb) {
      return jest.genMockFn().mockImplementation(function (/*err, user, info*/) {
        cb(undefined, user);
      });
    });

    login(req, res, next);

    expect(res.json).toBeCalled();
    expect(res.json.mock.calls[0][0].user).toBeDefined();
    expect(user.get).toBeCalled();
  });

  it('should return a token on successful login', function () {
    passport.authenticate.mockImplementation(function (strategy, options, cb) {
      return jest.genMockFn().mockImplementation(function (/*err, user, info*/) {
        cb(undefined, user);
      });
    });

    login(req, res, next);

    expect(res.json).toBeCalled();
    expect(res.json.mock.calls[0][0].token).toBeDefined();
    expect(user.generateToken).toBeCalled();
  });

});

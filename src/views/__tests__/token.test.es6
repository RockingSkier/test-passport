'use strict';

jest.dontMock('../token');


describe('token view', function() {
  let token;
  let req, res;

  beforeEach(function () {
    token = require('../token');

    req = {
      user: {
        get: jest.genMockFn(),
        generateToken: jest.genMockFn()
      }
    };

    res = {
      json: jest.genMockFn()
    };
  });

  it('should call the response', function() {
    token(req, res);

    expect(res.json).toBeCalled();
  });

  it('should retrieve the user\'s username', function () {
    token(req, res);

    expect(req.user.get).toBeCalled();
    expect(req.user.get).toBeCalledWith('username');
  });

  it('should retrieve the user\'s jwt token', function () {
    token(req, res);

    expect(req.user.generateToken).toBeCalled();
  });

});

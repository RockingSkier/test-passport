'use strict';


export default function token (req, res) {
  // Assumes passport auth middleware has run
  return res
    .json({
      user: { username: req.user.get('username')},
      token: req.user.generateToken()
    });
}

'use strict';

import bodyParser from 'body-parser';
import cors from 'cors';
import errorhandler from 'errorhandler';
import express from 'express';
import passport from 'passport';
import responseTime from 'response-time';

import db from 'db';
import models from 'models';
import routers from 'routers';
import strategies from 'strategies';

let app = express();

app.use(responseTime());
app.disable('x-powered-by');
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(errorhandler({ dumpExceptions: true, showStack: true }));

// Register Models
models(db);

// Use Passport for authentication
passport.use('anonymous', strategies.anonymous);
passport.use('jwt', strategies.jwt);
passport.use('local', strategies.local);
app.use(passport.initialize());

// Register routes
app.use(routers.login);
app.use(routers.token);

export default app;

'use strict';

import express from 'express';
import passport from 'passport';

import token from 'views/token';


let router = express.Router();

router.post('/token',
  passport.authenticate('jwt', { session: false }),
  token
);

export default router;

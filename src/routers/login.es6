'use strict';

import express from 'express';

import login from 'views/login';


let router = express.Router();

router.post('/login',
  login
);

export default router;

'use strict';

import expect from 'expect.js';
import config from 'config';
import knex from 'knex';
import Seeder from 'knex/lib/seed';
import supertest from 'supertest';

import app from 'app';
import knexfile from 'knexfile';

let knex_ = knex(knexfile[config.get('databaseConfig')]);
let seeder = new Seeder(knex_);


describe('login api', function () {
  let api;
  let loginUrl = '/login';

  let validMessage = function (msg) {
    return function (res) {
      expect(res.body).to.have.property('message');
      expect(res.body.message).to.be.an('string');
      expect(res.body.message).to.equal(msg);
    };
  };

  before(function () {
      // Reset DB
      seeder.run();
  });

  beforeEach(function () {
    // Create an app with session
    api = supertest(app);
  });

  describe('logging in', function () {

    it('should accept POST', function (done) {
      api
        .post(loginUrl)
        .expect(400)
        .end(done);
    });

    describe('failure', function () {

      it('should reject no credentials', function (done) {
        api
          .post(loginUrl)
          .send({})
          .expect(400)
          .expect(validMessage('Missing credentials'))
          .end(done);
      });

      it('should reject invalid usernames', function (done) {
        api
        .post(loginUrl)
        .send({
          username: [],
          password: 'pass'
        })
        .expect(400)
        .expect(validMessage('Missing credentials'))
        .end(done);
      });

      it('should reject wrong usernames', function (done) {
          api
          .post(loginUrl)
          .send({
            username: 'wrongUsername',
            password: 'pass'
          })
          .expect(400)
          .expect(validMessage('username and password combination is invalid'))
          .end(done);
      });

      it('should reject invalid passwords', function (done) {
          api
          .post(loginUrl)
          .send({
            username: 'user',
            password: []
          })
          .expect(400)
          .expect(validMessage('Missing credentials'))
          .end(done);
      });

      it('should reject wrong passwords', function (done) {
          api
          .post(loginUrl)
          .send({
            username: 'user',
            password: 'wrongPassword'
          })
          .expect(400)
          .expect(validMessage('username and password combination is invalid'))
          .end(done);
      });

    });

    describe('success', function () {

      it('should return a jwt token', function (done) {
          api
          .post(loginUrl)
          .send({
            username: 'user',
            password: 'pass'
          })
          .expect(200)
          .end(done);
      });

      it('should return a jwt token', function (done) {
          api
          .post(loginUrl)
          .send({
            username: 'user',
            password: 'pass'
          })
          .expect(200)
          .end(done);
      });

    });

  });

});

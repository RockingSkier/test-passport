'use strict';

import expect from 'expect.js';
import config from 'config';
import knex from 'knex';
import Seeder from 'knex/lib/seed';
import supertest from 'supertest';

import app from 'app';
import jwt from 'jwt';
import knexfile from 'knexfile';

let knex_ = knex(knexfile[config.get('databaseConfig')]);
let seeder = new Seeder(knex_);


describe('token api', function () {
  let api;
  let tokenUrl = '/token';

  before(function () {
      // Reset DB
      seeder.run();
  });

  beforeEach(function () {
    // Create an app with session
    api = supertest(app);
  });

  describe('refreshing token', function () {

    describe('reject', function () {

      it('should require a JWT token', function (done) {
        api
        .post(tokenUrl)
        .expect(401)
        .end(done);
      });

      it('should require a valid JWT token', function (done) {
        api
        .post(tokenUrl)
        .set('Authorization', 'JWT IAmNotAValidJwtToken')
        .expect(401)
        .end(done);
      });

      it('should reject expired JWT tokens', function (done) {
        let jwtToken = jwt.generate({ sub: 1 }, { expiresIn: 1 });
        setTimeout(function () {
          api
          .post(tokenUrl)
          .set('Authorization', `JWT ${jwtToken}`)
          .expect(401)
          .end(done);

        }, 1250);
      });

      it('should reject tokens with invalid payloads', function (done) {
        let jwtToken = jwt.generate({ animal: true });
        api
        .post(tokenUrl)
        .set('Authorization', `JWT ${jwtToken}`)
        .expect(401)
        .end(done);
      });

    });

    describe('accept', function () {

      let validToken = function (res) {
        expect(res.body).to.have.property('token');
        expect(res.body.token).to.be.an('string');
      };

      let validUser = function (res) {
        expect(res.body).to.have.property('user');
        expect(res.body.user).to.be.an('object');
        expect(res.body.user).to.have.property('username');
        expect(res.body.user.username).to.be.a('string');
      };

      it('should grant access to valid tokens', function (done) {
        let jwtToken = jwt.generate({ sub: 1 });
        api
        .post(tokenUrl)
        .set('Authorization', `JWT ${jwtToken}`)
        .expect(200)
        .end(done);
      });

      it('should return a token', function (done) {
        let jwtToken = jwt.generate({ sub: 1 });
        api
        .post(tokenUrl)
        .set('Authorization', `JWT ${jwtToken}`)
        .expect(200)
        .expect(validToken)
        .end(done);
      });

      it('should return a user', function (done) {
        let jwtToken = jwt.generate({ sub: 1 });
        api
        .post(tokenUrl)
        .set('Authorization', `JWT ${jwtToken}`)
        .expect(200)
        .expect(validUser)
        .end(done);
      });

    });

  });

});

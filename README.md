# Passport Test

A small application to test [PassportJS](http://passportjs.org/) authentication.

By Ben

*Because learning.*


# Install

```bash
 npm install            # Install dependencies
```

# Run

```bash
npm serve
```

There is one endpoint, `/token` that can be `POST`ed to in two ways.


Either login with credentials:
```json
{
    "username": "user",
    "password": "pass"
}
```

Or refresh the jwt token with the Authorization header:
```
Authorization: JWT JWT_TOKEN_GOES_HERE
```

Both responses return a user and token:
``` json
{
    "user": {
      "username": "user"
    },
    "token": "JWT_TOKEN_APPEARS_HERE"
}
```


# Test

```bash
npm test
```

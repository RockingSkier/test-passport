'use strict';

require('babel/register');

let http = require('http');

let config = require('config');

let app = require('./src/app');


let httpServer = http.createServer(app);
httpServer.listen(config.get('serve.port'));
httpServer.on('listening', function () {
    console.log('Listening on: ' + httpServer.address().port);
});

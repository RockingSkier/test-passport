'use strict';

import expect from 'expect.js';
import sinon from 'sinon';

import authenticate from 'strategies/jwt/authenticate';


describe('authenticate strategy', function () {
  let done;
  let jwtPayload;

  beforeEach(function () {
    done = sinon.spy();
    jwtPayload = {
      sub: 1
    };
  });

  it('should return bubble any errors up', function () {
    authenticate(jwtPayload, done);

    // expect done to be called with (error)
    expect(false).to.be(true);
  });

  it('should not check the database if no id is provided', function () {
    authenticate(jwtPayload, done);

    // expect user.fetch to be called
    expect(false).to.be(true);
  });

  it('should check the database if an id if provided', function () {
    authenticate(jwtPayload, done);

    // expect user.fetch to be called
    expect(false).to.be(true);
  });

  it('should return a user if available', function () {
    authenticate(jwtPayload, done);
    // exect done to be called with (null, USER_FROM_DB)

    expect(false).to.be(true);
  });

  it('should return false if no user exists', function () {
    jwtPayload.id = null;

    authenticate(jwtPayload, done);

    // expect done to be called with (null, false)
    expect(false).to.be(true);
  });

});

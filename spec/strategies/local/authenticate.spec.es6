'use strict';

import expect from 'expect.js';
import sinon from 'sinon';

import authenticate from 'strategies/jwt/authenticate';


describe('authenticate strategy', function () {
  let done;
  let jwtPayload;

  beforeEach(function () {
    done = sinon.spy();
    jwtPayload = {
      id: 1
    };
  });

  it('should return bubble any errors up', function () {
    authenticate(jwtPayload, done);

    // expect done to be called with (error)
    expect(false).to.be(true);
  });

  it('should check the database for a user', function () {
    authenticate(jwtPayload, done);

    // expect user.fetch to be called
    expect(false).to.be(true);
  });

  it('should return false if no user exists', function () {
    jwtPayload.id = null;

    authenticate(jwtPayload, done);

    // expect done to be called with (null, false)
    expect(false).to.be(true);
  });

  it('should compare the given password with the uesr\s password', function () {
    expect(true).to.be(true);
  });

  it('should return a user if passwords match', function () {
    authenticate(jwtPayload, done);

    // exect done to be called with (null, USER_FROM_DB)
    expect(false).to.be(true);
  });

  it('should return false if passwords do not match', function () {
    authenticate(jwtPayload, done);

    // exect done to be called with (null, USER_FROM_DB)
    expect(false).to.be(true);
  });

});

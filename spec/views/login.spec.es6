'use strict';

import expect from 'expect.js';
import sinon from 'sinon';

import login from 'views/login';


describe('login view', function () {
  let req = sinon.spy();
  let res = sinon.spy();
  let next = sinon.spy();

  it('should authenticate the user', function () {
    login(req, res, next);

    // Because err
    expect(next.called).to.be(true);
  });

});

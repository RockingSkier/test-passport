'use strict';

import expect from 'expect.js';
import sinon from 'sinon';

import token from 'views/token';


describe('token view', function () {
  let req, res;

  beforeEach(function () {
      req = {
        user: {
          get: sinon.spy(),
          generateToken: sinon.spy()
        }
      };

      res = {
        json: sinon.spy()
      };
  });

  it('should return successfully', function () {
    token(req, res);

    expect(res.json.called).to.be(true);
  });

  it('should retrieve the user\'s username', function () {
    token(req, res);

    expect(req.user.get.called).to.be(true);
    expect(req.user.get.calledWith('username')).to.be(true);
  });

  it('should retrieve the user\'s jwt token', function () {
    token(req, res);

    expect(req.user.generateToken.called).to.be(true);
  });

});

exports.up = function(knex, Promise) {
  'use strict';

  return knex.schema.createTable('users', function (table) {
    table.increments('id').primary();
    table.timestamps();
    table.string('username');
    table.string('password');
  });
};

exports.down = function(knex, Promise) {
  'use strict';

  return knex.schema
    .dropTable('users');
};
